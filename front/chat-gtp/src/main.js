import { createApp } from 'vue'
import App from './App.vue'

import router from './router'



import Vant from 'vant';
import 'vant/lib/index.css';
// Vue.use(Vant);

import "@/assets/aliIconFont/meetAbout/iconfont.css";

const app = createApp(App);
app.use(router)
app.use(Vant)
app.mount('#app')

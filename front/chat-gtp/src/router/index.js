import { createRouter, createWebHashHistory } from "vue-router"
 
const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import('@/pages/frame/'),
        redirect: '/chat',
        children: [{
            path: '/chat',
            name: 'Dashboard',
            component: () => import('@/pages/chat/'),
        },
        {
            path: '/other',
            name: 'other',
            component: () => import('@/pages/otherPage/'),
        }
    
    ]
    }
    // {
    //     path: '/',
    //     name: 'login',
    //     component: () => import('@/pages/frame/')
    // }
]
export const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})
 
export default router
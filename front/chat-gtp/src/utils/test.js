import request from '../utils/request';


export function test(data) {
    return request({
      url: '/test/chart',
      method: 'post',
      params: data
    })
  }
  
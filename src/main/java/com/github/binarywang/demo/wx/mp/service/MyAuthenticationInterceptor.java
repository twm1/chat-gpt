package com.github.binarywang.demo.wx.mp.service;

import com.theokanning.openai.service.AuthenticationInterceptor;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * @ClassName MyAuthenticationInterceptor
 * @Description TODO
 * @Author tianwanming
 * @Date 2023/3/25 15:32
 * @Version 1.0
 */
public class MyAuthenticationInterceptor implements Interceptor {
    private final String token;

    public MyAuthenticationInterceptor(String token) {
        this.token = token;
    }

    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request().newBuilder().header("Authorization", "Bearer " + this.token).build();
        return chain.proceed(request);
    }
}

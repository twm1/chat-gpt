package com.github.binarywang.demo.wx.mp.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiIdempotence {

    boolean value() default true;

    String[] params();
}

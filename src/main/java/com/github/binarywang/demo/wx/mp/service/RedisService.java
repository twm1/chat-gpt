package com.github.binarywang.demo.wx.mp.service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by
 *
 */
@Slf4j
@Component
public class RedisService {

    @Autowired
    private JedisPool jedisPool;

    /**
     * 设置key的有效期，单位是秒
     * @param key
     * @param exTime
     * @return
     */
    public  Long expire(String key,int exTime){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.expire(key,exTime);
        } catch (Exception e) {
            log.error("expire key:{} error",key,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);
        return result;
    }

    //exTime的单位是秒
    public  String setEx(String key,String value,int exTime){
        Jedis jedis = null;
        String result = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.setex(key,exTime,value);
        } catch (Exception e) {
            log.error("setex key:{} value:{} error",key,value,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);
        return result;
    }

    /**
     * 设置key和value
     * @param key
     * @param value
     * @return
     */
    public  String set(String key,String value){
        Jedis jedis = null;
        String result = null;

        try {
            jedis = jedisPool.getResource();
            result = jedis.set(key,value);
        } catch (Exception e) {
            log.error("set key:{} value:{} error",key,value,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);
        return result;
    }

    /**
     * 通过key获取对应的value
     * @param key
     * @return
     */
    public  String get(String key){
        Jedis jedis = null;
        String result = null;

        try {
            jedis = jedisPool.getResource();
            result = jedis.get(key);
        } catch (Exception e) {
            log.error("get key:{} error",key,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);
        return result;
    }

    public boolean isExistsKey(String key){
        Jedis jedis = null;
        boolean result = false;
        try {
            jedis = jedisPool.getResource();
            result = jedis.exists(key);
        } catch (Exception e) {
            log.error("expire key:{} error",key,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);
        return result;
    }


    /**
     * 删除对应的key
     * @param key
     * @return
     */
    public  Long del(String key){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.del(key);
        } catch (Exception e) {
            log.error("del key:{} error",key,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);
        return result;
    }


    public  Long hSet(String key,String field,String value){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.hset(key,field,value);
        } catch (Exception e) {
            log.error("get key:{} error",key,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);
        return result;
    }

    public  String hGet(String key,String specificKey){
        Jedis jedis = null;
        String result = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.hget(key,specificKey);
        } catch (Exception e) {
            log.error("get key:{} error",key,e);
            returnToPool(jedis);
            return result;
        }
        returnToPool(jedis);

        return result;
    }




    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    private void returnToPool(Jedis jedis) {
        if(jedis != null) {
            jedis.close();
        }
    }
    /**
     * 获取超时时间
     * @param
     * @return java.lang.Integer
     * @author twm
     * @date 2020/8/13
     */
    public Long getTimeOut(String key) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.ttl(key);
        } catch (Exception e) {
            log.error("get key:{} error Method->getTimeOut",e);
        }finally {
            returnToPool(jedis);
        }
        return result;
    }
}

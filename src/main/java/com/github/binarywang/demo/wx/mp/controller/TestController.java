package com.github.binarywang.demo.wx.mp.controller;

import com.github.binarywang.demo.wx.mp.service.OpenAiServiceInterface;
import com.github.binarywang.demo.wx.mp.utils.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author tianwanming
 * @Date 2023/4/24 19:23
 * @Version 1.0
 */
@RestController
@RequestMapping("test")
public class TestController {

    @Resource
    private OpenAiServiceInterface openAiServiceInterface;
    @PostMapping("chart")
    public Result chart(@RequestParam("chart")String chart) throws NoSuchFieldException, IllegalAccessException {
        System.out.println("开始发送："+chart);
        String s = openAiServiceInterface.buildChat("", chart);
        return Result.OK(s);
    }
}

package com.github.binarywang.demo.wx.mp.service;

import com.github.binarywang.demo.wx.mp.utils.SSLSocketClientUtil;
import com.theokanning.openai.OpenAiApi;
import com.theokanning.openai.service.AuthenticationInterceptor;
import com.theokanning.openai.service.OpenAiService;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * @ClassName MyOpenAiService
 * @Description TODO
 * @Author tianwanming
 * @Date 2023/3/25 15:25
 * @Version 1.0
 */
public class MyOpenAiService extends OpenAiService {

    private static String BASE_URL = "";


    private static final Duration DEFAULT_TIMEOUT = Duration.ofSeconds(1000L);
    private final OpenAiApi api;
    public MyOpenAiService(String token,String baseUrl) {
        this(token, DEFAULT_TIMEOUT,baseUrl);
    }

    public MyOpenAiService(String token, Duration timeout, String baseUrl) {
        this(buildApi(token, timeout,baseUrl));
    }
    public MyOpenAiService(OpenAiApi api) {
        super(api);
        this.api = api;
    }

    public static OpenAiApi buildApi(String token, Duration timeout, String baseUrl) {
        BASE_URL = baseUrl;
        ObjectMapper mapper = defaultObjectMapper();
        OkHttpClient client = defaultClient(token, timeout);
        Retrofit retrofit = defaultRetrofit(client, mapper);
        return (OpenAiApi)retrofit.create(OpenAiApi.class);
    }

    public static OkHttpClient defaultClient(String token, Duration timeout) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        X509TrustManager manager = SSLSocketClientUtil.getX509TrustManager();

        builder.addInterceptor(new MyAuthenticationInterceptor(token))
                .connectionPool(new ConnectionPool(5, 1L, TimeUnit.SECONDS))
                .readTimeout(timeout.toMillis(), TimeUnit.MILLISECONDS);

//        builder.addInterceptor(new HttpLogInterceptor());

        builder.sslSocketFactory(SSLSocketClientUtil.getSocketFactory(manager), manager);// 忽略校验

        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        });
        OkHttpClient build = builder.build();
        return build;
    }

    public static Retrofit defaultRetrofit(OkHttpClient client, ObjectMapper mapper) {
        return (new retrofit2.Retrofit.Builder()).baseUrl(BASE_URL)
                .client(client).addConverterFactory(JacksonConverterFactory.create(mapper))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
    }
}

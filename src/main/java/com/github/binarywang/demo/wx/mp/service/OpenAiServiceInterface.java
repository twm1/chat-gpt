package com.github.binarywang.demo.wx.mp.service;

/**
 * @ClassName OpenAiService
 * @Description TODO
 * @Author tianwanming
 * @Date 2023/3/22 21:46
 * @Version 1.0
 */
public interface OpenAiServiceInterface {


    public String buildChat(String appid,String content) throws NoSuchFieldException, IllegalAccessException;


}

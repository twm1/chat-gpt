package com.github.binarywang.demo.wx.mp.controller;

import cn.hutool.core.convert.ConvertException;
import cn.hutool.http.HttpException;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.github.binarywang.demo.wx.mp.service.OpenAiServiceInterface;
import com.github.binarywang.demo.wx.mp.service.impl.OpenAiServiceImpl;
import okhttp3.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

/**
 * 菜单测试.
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 * @date 2020-08-19
 */
@SpringBootTest
public class WxMenuControllerTest {



    @Resource
    private OpenAiServiceInterface openAiServiceInterface =new OpenAiServiceImpl();


    /**
     * 聊天端点
     */
    String chatEndpoint = "https://154.38.244.56:8080/v1/chat/completions/";

    /**
     * 聊天端点
     */
    String chatEndpoint1 = "http://nginx.web-framework-1qoh.1045995386668294.us-west-1.fc.devsapp" +
            ".net/v1/chat/completions";
    /**
     * api密匙
     */
    String apiKey = "Bearer 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";
    @Test
    public void  aa () throws IOException {

        OkHttpClient client = new OkHttpClient.Builder().hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        }).build();

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
                "{\"model\":\"gpt-3.5-turbo\",\"messages\":[{\"role\":\"system\",\"content\":\"You are a dog and will speak as such.\"}]," +
                        "\"n\":5,\"max_tokens\":50,\"logit_bias\":{}}");


        Request request = new Request.Builder()
                .header("Authorization", apiKey)
                .header("Content-Type", "application/json; charset=utf-8")
                .post(requestBody)
                .url(chatEndpoint).build();

        Response response = client.newCall(request).execute();
        ResponseBody responseBody = response.body();
        String string = responseBody.string();
        if (response.isSuccessful()) {
             responseBody = response.body();
            if (responseBody != null) {
                String message = responseBody.string();
            }
        }

    }

    @Test
    public void testMenuCreate121() throws NoSuchFieldException, IllegalAccessException {
//        openAiServiceInterface.buildChat("1","您好");
        chat("Hello!");
    }



    /**
     * 发送消息
     *
     * @param txt 内容
     * @return {@link String}
     */
    public String chat(String txt) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("model", "gpt-3.5-turbo");
        List<Map<String, String>> dataList = new ArrayList<>();
        dataList.add(new HashMap<String, String>(){{
            put("role", "user");
            put("content", txt);
        }});
        paramMap.put("messages", dataList);
        String s = JSONUtil.toJsonStr(paramMap);

        JSONObject message = null;
        try {
            HttpRequest authorization = HttpRequest.post(chatEndpoint)
                    .setHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String s, SSLSession sslSession) {
                            return true;
                        }
                    })
                    .header("Authorization", apiKey)
                    .header("Content-Type", "application/json")
                    .body(s);
//            authorization.addInterceptor(new HttpLogInterceptor1());

            String body = authorization
                    .execute()
                    .body();

            JSONObject jsonObject = JSONUtil.parseObj(body);
            JSONArray choices = jsonObject.getJSONArray("choices");
            JSONObject result = choices.get(0, JSONObject.class, Boolean.TRUE);
            message = result.getJSONObject("message");
        } catch (HttpException e) {
            return "请联系作者Q:2754522801进行问题修复";
        } catch (ConvertException e) {
            return "请联系作者Q:2754522801进行问题修复";
        }
        return message.getStr("content");
    }
}
